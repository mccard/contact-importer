class Contact < ApplicationRecord
  validates :name, presence: true, format: { with: /\A([a-z\d\s\/-]*)\z/i }
  validates :date_of_birth, presence: true, format: { with: /\A[0-9][0-9][0-9][0-9]-((0[1-9])|(1[0-2]))-([0-2][1-9]|3[0-1])\z/i }
  validates :address, presence: true
  validates :phone, presence: true, format: { with: /\A(\(\+[0-9][0-9]\))\s(\d{3})(-|\s)(\d{3})(-|\s)(\d{2})(-|\s)(\d{2})\z/i }
  validates :credit_card, presence: true

  encrypts :credit_card

  validate :credit_card_validation

  before_save :populate_franchise

  private

  def credit_card_validation
    errors.add(:credit_card, :invalid) unless credit_card.creditcard?
  end

  def populate_franchise
    self.franchise = credit_card.creditcard_type
  end
end


# credit_card.split(//).last(4).join

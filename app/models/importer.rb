require 'csv'

class Importer < ApplicationRecord
  has_many_attached :files

  def create_contacts
    path = path = ActiveStorage::Blob.service.send(:path_for, files.first.key)

    CSV.foreach path, headers: true do |row|
      Contact.create!(row.to_hash)
    end
  end
end

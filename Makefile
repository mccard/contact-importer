GREEN=\033[0;32m
BLUE=\033[0;34m
NC=\033[0m

build:
	@echo "${GREEN}Building containers... ${NC}"
	docker-compose up -d

boot: build
	@echo "${GREEN}Creating database... ${NC}"
	docker-compose exec web rails db:create
	@echo "${GREEN}Running migrations... ${NC}"
	docker-compose exec web rails db:migrate
	@echo "${BLUE}Tip: From now on, you can simply start the server with the command 'make up'.${NC}"
	@echo "${GREEN}Build finished. The server is now listening on port 3000!${NC}"
	@docker attach contact_importer

up: build
	@echo "${GREEN}Just a minute..${NC}"
	@docker attach contact_importer

rspec: build
	docker-compose exec web rspec spec

console: build
	docker-compose exec web rails c

bash: build
	docker exec -it contact_importer bash

down:
	docker-compose down

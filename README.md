
# Contact-Importer

Contact-Importer is a simple app to import contact information via csv

## Setup

### Local

- Ruby 3.0.0 (it is recommended that you use [rvm](https://rvm.io/) or [rbenv](https://github.com/rbenv/rbenv))
- [PostgreSQL](https://www.postgresql.org/download/)

### Docker

- [Docker](https://docs.docker.com/get-docker/)
- [docker-compose](https://docs.docker.com/compose/install/)

## Usage

Clone this repo and make sure you're inside of it

```bash
git clone git@github.com:mccard/contact_importer.git
cd contact_importer
```


### Local

```bash
# Install bundler gem
gem install bundler:2.2.7

# Install project gems
bundle install

# Create database structure
bundle exec rake db:create
bundle exec rake db:migrate

# Start the server!
bundle exec rails s

# You may wanna check if contact_importer is running by accessing http://localhost:3000!
```

### With Docker

```bash
# To make things easier, I have already setup a Makefile with the basic commands
# to be run with docker

# If it is your first time running the project, simply run the following
# command and see the magic happen!
# This will build the containers, setup the database and start the server!
make boot

# You may wanna check if contact_importer is running by accessing http://localhost:3000!

# From now on, you can simply run the server with the following command
make up

# To access the rails console
make console

# To have bash access
make bash

# Make sure you run this to stop and remove the containers once you're done coding!
make down
```

## Testing

### Local

Simply run the following command on project root

```bash
  bundle exec rspec
```

### With Docker

Simply run the following command on project root

```bash
  make rspec
```

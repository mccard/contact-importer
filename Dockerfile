FROM ruby:3.0.0

# Create directory
RUN mkdir /app

# Define the working directory
WORKDIR /app

# Copy Gemfile to container
COPY Gemfile /app/Gemfile
COPY Gemfile.lock /app/Gemfile.lock

# Install bundler gem
RUN gem install bundler -v 2.2.7

# Install gems
RUN bundle install

# Copy code to container
COPY . /app
